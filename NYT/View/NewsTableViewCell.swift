//
//  NewsTableViewCell.swift
//  NYT
//
//  Created by makar4ik on 25.10.2019.
//  Copyright © 2019 makar4ik. All rights reserved.
import SDWebImage
import UIKit

class NewsTableViewCell: UITableViewCell {
    var coreDataManager = CoreDataManager()
    
    var news: News? {
        didSet {
            guard let unwrapAftor = news?.aftor,
                let unwrapImage = news?.imageURL,
                let unwrapAbstract = news?.abstract,
                let unwrapTitle = news?.title else { return }
            
            cellImage.sd_setImage(with: unwrapImage, completed: nil)
            aftor.text = unwrapAftor
            abstract.text = unwrapAbstract
            title.text = unwrapTitle
        }
    }
    
    var button: UIButton = {
        let myButton = UIButton()
        myButton.translatesAutoresizingMaskIntoConstraints = false
        myButton.setImage(#imageLiteral(resourceName: "StarOff"), for: .normal)

        return myButton
    }()
    
    var cellImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.cornerRadius = 12
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.layer.masksToBounds = true

        return image
    }()
    
    var aftor: UILabel = {
        let aftor = UILabel()
        aftor.translatesAutoresizingMaskIntoConstraints = false
        aftor.font = UIFont.boldSystemFont(ofSize: 8)
        aftor.textAlignment = .right
        aftor.numberOfLines = 0
        aftor.sizeToFit()
        
        return aftor
    }()
    
    var abstract: UILabel = {
        let abstTitle = UILabel()
        abstTitle.translatesAutoresizingMaskIntoConstraints = false
        abstTitle.font = UIFont.systemFont(ofSize: 9)
        abstTitle.textAlignment = .left
        abstTitle.numberOfLines = 0
        abstTitle.sizeToFit()
     
        return abstTitle
    }()
    
    var title: UILabel = {
        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = UIFont.boldSystemFont(ofSize: 10)
        title.textAlignment = .center
        title.numberOfLines = 0
        title.sizeToFit()
        
        return title
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setupAllConstraints()
    }
    
    private func setupAllConstraints() {
        // Add Button Constraint
        button.addTarget(self, action: #selector(addFavoriteNews), for: .touchUpInside)
        addSubview(button)
        
        button.topAnchor.constraint(equalTo: topAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.widthAnchor.constraint(equalToConstant: 30).isActive = true
        button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        // Add CellImage Constraint
        addSubview(cellImage)
        
        cellImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        cellImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        cellImage.widthAnchor.constraint(equalToConstant: 75).isActive = true
        cellImage.heightAnchor.constraint(equalToConstant: 75).isActive = true
        
        // Add Title Constraint
        addSubview(title)
        
        title.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        title.leadingAnchor.constraint(equalTo: cellImage.trailingAnchor, constant: 8).isActive = true
        title.trailingAnchor.constraint(equalTo: button.leadingAnchor, constant: -8).isActive = true
        
        // Add Abstract Constraint
        addSubview(abstract)
        
        abstract.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 3).isActive = true
        abstract.leadingAnchor.constraint(equalTo: cellImage.trailingAnchor, constant: 5).isActive = true
        abstract.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        // Add Aftor Constraint
        addSubview(aftor)
     
        aftor.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        aftor.heightAnchor.constraint(equalToConstant: 12).isActive = true
        aftor.widthAnchor.constraint(equalToConstant: frame.size.width/2).isActive = true
        aftor.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    // After click button save news from CoreData and setup new image from butomImage
    @objc func addFavoriteNews(){
        let butomImage = button.imageView!.image
        
        if butomImage == #imageLiteral(resourceName: "StarOff") {
            button.setImage(#imageLiteral(resourceName: "StarOn"), for: .normal)
           
            guard let unwrapNews = news else { return }
            coreDataManager.saveNews(news: unwrapNews)
        }
    }
    
    //If News is in CoreData then change the image
    func sortedFavoriteNews(dataMasive: [News]){
        guard let unwrapNews = news else { return }
        
        if dataMasive.contains(where: {$0.id == unwrapNews.id }) {
            button.setImage(#imageLiteral(resourceName: "StarOn"), for: .normal)
        } else {
        }
    }
}
