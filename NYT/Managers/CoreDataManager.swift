//
//  CoreDataManager.swift
//  NYT
//
//  Created by makar4ik on 27.10.2019.
//  Copyright © 2019 makar4ik. All rights reserved.
//

import CoreData
import Foundation
import UIKit

class CoreDataManager: NSObject {
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let fetchRequest: NSFetchRequest<ModelNews> = ModelNews.fetchRequest()
    
    func deleteNews(id: Int64, completionHandler: @escaping () -> ()) {
        let newId = String(id)
        fetchRequest.predicate = NSPredicate(format: "id = %@", newId)
        
        do {
            let results = try context.fetch(fetchRequest)
            guard let resultsNews = results.first else { return }
            
            context.delete(resultsNews)
            seveContext()
            completionHandler()
            
        } catch let error as NSError{
            print(error.userInfo)
        }
    }
    
    func fetchAllNews( completionHandler: @escaping (_ newsMasive: [News]?) -> ()) {
        var masive: [News]? = []
        
        do {
            let results = try context.fetch(fetchRequest)
            if results.isEmpty {
                completionHandler(nil)
            } else {
                masive = returnNewsMasiv(model: results)
                completionHandler(masive)
            }
        } catch let error as NSError{
            print(error.userInfo)
        }
    }
    
    func saveNews(news: News) {
        let myNews = ModelNews(context: context)
        myNews.imageURL = news.imageURL
        myNews.newsURL = news.newsURL
        myNews.id = news.id!
        myNews.abstract = news.abstract
        myNews.aftor = news.aftor
        myNews.title = news.title
        
        seveContext()
    }
    
    private func seveContext() {
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    private func returnNewsMasiv(model: [ModelNews]) -> [News]{
        var masiveNews: [News] = []
        
        for news in model {
            masiveNews.append(News(model: news))
        }
        return masiveNews
    }
}
