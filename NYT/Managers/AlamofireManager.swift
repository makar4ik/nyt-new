import UIKit
import Alamofire
import SwiftyJSON

class AlamofireManager: NSObject {
    
    func fetchNews(model: ModelURL, completionHandler: @escaping (_ newsMasive: [News]) -> ()) {
        Alamofire.request(model.rawValue).responseJSON { response in
            guard response.result.isSuccess else {
                print("Ошибка при запросе данных \(String(describing: response.result.error))")
                return
            }
            guard let data = response.result.value  else { return }
            let masive = self.jsonReturnMasiveNews(json: JSON(data))
            
            completionHandler(masive)
        }
    }
    
    
    private func jsonReturnMasiveNews(json: JSON) -> [News] {
        var newsMasive = [News]()
        let jsonMasiv = json["results"]
        
        for (_, news) in jsonMasiv {
            let apendNews = News(json: news)
            newsMasive.append(apendNews)
        }
        return newsMasive
    }
}
