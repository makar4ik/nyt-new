//
//  NewsTableController.swift
//  NYT
//
//  Created by makar4ik on 25.10.2019.
//  Copyright © 2019 makar4ik. All rights reserved.

import SafariServices
import Foundation
import UIKit

class NewsTableController: UITableViewController {
    
    private var alamofireManager = AlamofireManager()
    private var newsMasive: [News] = []
    private var coreDataManager = CoreDataManager()
    private var coreDataMasive: [News] = []
    var index: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.tableFooterView = UIView()
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: Id.cellId)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Favorite"), style: .plain, target: self, action: #selector(showFavoriteController))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = false
        uploadCoreData ()
        
        if newsMasive.isEmpty {                 //Upload News for API
            view.addBlurEffect()
            navigationController?.view.addActivityIndicatorAndStart()
            
            alamofireManager.fetchNews(model: index!.returnModel) { masive in
                self.newsMasive = masive
                
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.navigationController?.view.stopActivityIndicator()
                    self.view.removeBlurEffect()
                }
            }
        } else {
            tableView.reloadData()
        }
    }
    
   private func uploadCoreData () {               // Upload CoreData
        coreDataManager.fetchAllNews { (masive) in
            guard let unrapNewsMasive = masive else { return }
            self.coreDataMasive = unrapNewsMasive
        }
    }
    
    @objc func showFavoriteController() {        //Show FavoriteTableController
        let favoriteVC = FavoriteTableController()
        navigationController?.pushViewController(favoriteVC, animated: true)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsMasive.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Id.cellId, for: indexPath) as! NewsTableViewCell

        cell.news = newsMasive[indexPath.row]
        cell.button.setImage(#imageLiteral(resourceName: "StarOff"), for: .normal)
        cell.sortedFavoriteNews(dataMasive: coreDataMasive)

        return cell
    }
    
    // After select add and show SafariViewController
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url =  newsMasive[indexPath.row].newsURL else { return }
        
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        
        let safariVC = SFSafariViewController(url: url, configuration: config)
        present(safariVC, animated: true, completion: nil)
    }
}
