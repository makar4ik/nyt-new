//
//  FavoriteTableController.swift
//  NYT
//
//  Created by makar4ik on 25.10.2019.
//  Copyright © 2019 makar4ik. All rights reserved.
//

import SafariServices
import UIKit

class FavoriteTableController: UITableViewController {
   
    private var coreDataManager = CoreDataManager()
    private var newsMasive: [News] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Favorite"
        tabBarController?.tabBar.isHidden = true
        tableView.tableFooterView = UIView()
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: Id.cellId)
        
        coreDataManager.fetchAllNews { (masive) in            //Upload News from CorData
            guard let unrapNewsMasive = masive else { return }
            self.newsMasive = unrapNewsMasive
           
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsMasive.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Id.cellId, for: indexPath) as! NewsTableViewCell
        
        cell.button.isHidden = true
        cell.news = newsMasive[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let news = newsMasive[indexPath.row]
        guard let newId = news.id, editingStyle == .delete else { return }
      
        coreDataManager.deleteNews(id: newId) {              //Remove News from CoreData
            self.newsMasive.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // After select add and show SafariViewController
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url =  newsMasive[indexPath.row].newsURL else { return }
        
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        
        let safariVC = SFSafariViewController(url: url, configuration: config)
        present(safariVC, animated: true, completion: nil)
    }
}

