//
//  TabBarController.swift
//  NYT
//
//  Created by makar4ik on 25.10.2019.
//  Copyright © 2019 makar4ik. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barStyle = .default
        
        let VC1 =  NewsTableController()
        VC1.index = 0
        VC1.navigationItem.title = "Email"
        let emailNC = UINavigationController(rootViewController: VC1)
        emailNC.tabBarItem.image = #imageLiteral(resourceName: "Email")
        emailNC.tabBarItem.title = "Email"
        
        let VC2 =  NewsTableController()
        VC2.index = 1
        VC2.navigationItem.title = "Shared"
        let sharedNC = UINavigationController(rootViewController: VC2)
        sharedNC.tabBarItem.image = #imageLiteral(resourceName: "Shared")
        sharedNC.tabBarItem.title = "Shared"
        
        
        let VC3 =  NewsTableController()
        VC3.index = 2
        VC3.navigationItem.title = "View"
        let viewNC = UINavigationController(rootViewController: VC3)
        viewNC.tabBarItem.image = #imageLiteral(resourceName: "View")
        viewNC.tabBarItem.title = "View"
        
        viewControllers = [emailNC, sharedNC, viewNC]
    }
    
}
