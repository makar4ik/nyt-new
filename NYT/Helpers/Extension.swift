//
//  Extentions.swift
//  NYT
//
//  Created by makar4ik on 26.10.2019.
//  Copyright © 2019 makar4ik. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    var returnModel: ModelURL {
       
        switch self {
        case 0 : return .email
        case 1 : return .shared
        case 2 : return .view
        default: break
        }
        return .email
    }
}

extension UIView {
    
    func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(blurEffectView)
    }
    
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
    
    func addActivityIndicatorAndStart() {
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame.size = CGSize(width: 40, height: 40)
        actInd.center = self.center
        actInd.hidesWhenStopped = true
        actInd.style = UIActivityIndicatorView.Style.whiteLarge
        actInd.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(actInd)
        actInd.startAnimating()
    }
    
    func stopActivityIndicator() {
        let activityIndicator = self.subviews.filter{$0 is UIActivityIndicatorView}
        activityIndicator.forEach{ activityInd in
            let activ = activityInd as! UIActivityIndicatorView
            activ.stopAnimating()
        }
    }
}
