import Foundation

enum ModelURL: String {
    case email = "https://api.nytimes.com/svc/mostpopular/v2/emailed/30.json?api-key=6R2AhOvp4ycw78n9fH2g6kBJpp2xAe9r"
    case shared = "https://api.nytimes.com/svc/mostpopular/v2/shared/30/facebook.json?api-key=6R2AhOvp4ycw78n9fH2g6kBJpp2xAe9r"
    case view = "https://api.nytimes.com/svc/mostpopular/v2/viewed/30.json?api-key=6R2AhOvp4ycw78n9fH2g6kBJpp2xAe9r"
}
