//
//  News.swift
//  NYT
//
//  Created by makar4ik on 26.10.2019.
//  Copyright © 2019 makar4ik. All rights reserved.
//
import UIKit
import Foundation
import SwiftyJSON

struct News: Decodable {
    var imageURL: URL?
    var newsURL: URL?
    var title: String?
    var aftor: String?
    var abstract: String?
    var id: Int64?
    
    init(model: ModelNews){
        self.imageURL = model.imageURL
        self.newsURL = model.newsURL
        self.title = model.title
        self.aftor = model.aftor
        self.abstract = model.abstract
        self.id = model.id
    }
    
    init(json: JSON){
        self.newsURL = json["url"].url
        self.title = json["title"].string
        self.aftor = json["byline"].string
        self.abstract = json["abstract"].string
        self.id = json["id"].int64
        self.imageURL = json["media"][0]["media-metadata"][0]["url"].url
    }
}
